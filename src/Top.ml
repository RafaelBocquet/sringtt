open Common

type entry = { index : top;
               ty    : Term.t;
               vty   : Value.t;
               tm    : Term.t;
               vtm   : Value.t;
               name  : string;
             }

type ctx = { array : entry CCVector.vector
           }

let create_ctx ()
  = { array = CCVector.create ()
    }

let lookup (tctx : ctx) (Top(i) : top)
  = CCVector.get tctx.array i

let push tctx ty vty tm vtm name =
  let i = Top (CCVector.size tctx.array) in
  CCVector.push tctx.array { index = i; ty; vty; tm; vtm; name; };
  i
