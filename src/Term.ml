open Common

type t
  = Var of idx
  | Meta of meta
  | Top of top
  | Let of t * t
  | Set
  (* Pi *)
  | Pi of icit * t * t
  | Lam of t
  | App of t * t * t
  (* Record *)
  | RecordType of record_sig
  | Record of record
  | Proj of t * string
  (* StrProp *)
  | StrPropType
  | StrProp of t
  | StrPropCarrier of t
  | Bottom
  | Absurd of t
  (* Ring *)
  | StrRingType
  | StrRing of t
  | StrRingCarrier of t
  | StrRingZero of t
  | StrRingOne of t
  | StrRingAdd of t
  | StrRingNeg of t
  | StrRingMul of t
  | StrRingConstant of t * Z.t
                    [@printer fun fmt (x,y) -> fprintf fmt "StrRingConstant(%a,%s)" pp x (Z.to_string y)]
  | StrRingPoly of poly_t
  | ZZ
  (* OTT / SeTT *)
  | Eq of t * t * t
  | Refl of t * t
  | Cast of t * t * t * t
[@@deriving show]

and record_sig =
  | SigEmpty
  | SigExtend of string * t * record_sig
[@@deriving show]

and record =
  | RecEmpty
  | RecExtend of string * t * record
[@@deriving show]

and poly_t = { ring : t;
               vars : t CCVector.vector;
               [@printer Util.pp_vector pp]
               poly : ZPoly.t;
             }
[@@deriving show]

let lams t n =
  let rec go t = function
  | 0 -> t
  | n -> go (Lam t) (n-1)
  in
  if n < 0 then failwith ("impossible " ^ __LOC__);
  go t n
