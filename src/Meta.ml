open Common

type entry = Solved of Value.t (* type *) * Term.t (* term *) * Value.t (* value *)
           | Unsolved of Value.t (* type *)

type ctx = { array : entry CCVector.vector
           }

let create_ctx () : ctx
  = { array = CCVector.create ();
    }

let lookup ctx (Meta(m) : meta) = CCVector.get ctx.array m

let get_type ctx (Meta(m) : meta) = match CCVector.get ctx.array m with
  | Unsolved(ty) -> ty
  | Solved(ty,_,_) -> ty

let fresh_meta ctx ty : meta =
  let m = CCVector.size ctx.array in
  CCVector.push ctx.array (Unsolved(ty));
  Meta(m)

let is_unsolved = function
  | Unsolved(_) -> true
  | _ -> false

let solve_meta ctx (Meta(m) : meta) t v = match CCVector.get ctx.array m with
  | Unsolved(ty) -> CCVector.set ctx.array m (Solved(ty,t,v))
  | _ -> failwith ("impossible " ^ __LOC__)
