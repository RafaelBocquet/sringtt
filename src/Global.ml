open Common

type ctx
  = { mctx : Meta.ctx;
      tctx : Top.ctx
    }

let create_ctx ()
  = { mctx = Meta.create_ctx ();
      tctx = Top.create_ctx ();
    }
