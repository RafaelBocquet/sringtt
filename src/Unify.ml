open Common
module Ctx = EvalCtx

exception UnifyError

let invert_spine (ctx : Ctx.ctx) sp : Renaming.t =
  let cnt = CCVector.make ctx.lvl 0 in
  let rec go0 : Value.spine -> unit = function
    | SEmpty -> ()
    | SApp(sp,_,v) ->
      go0 sp;
      begin match Eval.force ctx v with
        | Var(x,SEmpty) ->
          CCVector.set cnt x (CCVector.get cnt x + 1)
        | _ -> raise UnifyError
      end
    | SProj(_)
    | SAbsurd(_) ->
      raise UnifyError (* TODO *)
  in
  go0 sp;
  let ren    = Renaming.create ctx.lvl MetaSet.empty in
  let rec go1 : Value.spine -> unit = function
    | SEmpty -> ()
    | SApp(s,_,v) ->
      begin match Eval.force ctx v with
        | Var(x, SEmpty) ->
          go1 s;
          if CCVector.get cnt x = 1
          then Renaming.push ren x
          else Renaming.drop ren
        | _ -> failwith ("impossible " ^ __LOC__)
      end
    | SProj(_)
    | SAbsurd(_) ->
      failwith ("impossible " ^ __LOC__)
  in
  go1 sp;
  ren

let solve_meta ctx m sp v =
  Format.fprintf Format.std_formatter "Solving meta:\n%a %a@. = %a@."
    pp_meta m Value.pp_spine sp Value.pp v;
  let ren = invert_spine ctx sp in
  ren.cycles <- MetaSet.singleton m;
  if not (Renaming.is_trivial ren)
  then (let _ = Renaming.prune_type ctx.gctx ren (Meta.get_type ctx.gctx.mctx m) in ());
  let sol = Term.lams (Renaming.rename ctx ren v) ren.size in
  Meta.solve_meta ctx.gctx.mctx m sol (Eval.eval (Ctx.create ctx.gctx 0) EEmpty sol)

let solve_flex_flex ctx m1 sp1 v1 m2 sp2 v2 =
  try solve_meta ctx m1 sp1 v2
  with UnifyError ->
  try solve_meta ctx m2 sp2 v1
  with UnifyError ->
    raise UnifyError

let intersect (ctx : Ctx.ctx) st m sp1 sp2 =
  let ren = Renaming.create ctx.lvl (MetaSet.singleton m) in
  let exception NonLinearSpine in
  let rec go : Value.spine * Value.spine -> _ = function
    | SEmpty, SEmpty -> ()
    | SApp(sp1,_,v1), SApp(sp2,_,v2) ->
      begin match Eval.force ctx v1, Eval.force ctx v2 with
        | Var(x1, SEmpty), Var(x2, SEmpty) ->
          go (sp1,sp2);
          if x1 = x2
          then Renaming.push ren x1
          else Renaming.drop ren
        | _ -> raise NonLinearSpine
      end
    | _ -> raise NonLinearSpine
  in
  try
    go (sp1,sp2);
    if not (Renaming.is_trivial ren)
    then begin
      if st = UFlex then raise UnifyError;
      let _ = Renaming.prune_meta ctx.gctx m ren in ()
    end
  with NonLinearSpine -> raise UnifyError

let rec unify ctx (st : unify_state) s v1 v2 =
  (* Format.fprintf Format.std_formatter "unify %a@.with %a@.in %a@." Value.pp v1 Value.pp v2 Value.pp s; *)
  unify_ ctx st (Eval.force ctx s, Eval.force ctx v1, Eval.force ctx v2)

and unify_ ctx st : Value.t * Value.t * Value.t -> unit = function
  (* Pi *)
  | _, Pi(pl1,a1,b1), Pi(pl2,a2,b2) when pl1 = pl2 ->
    unify ctx st Set a1 a2;
    unify (Ctx.extend ctx) st Set (Eval.clAppVar ctx b1 ctx.lvl) (Eval.clAppVar ctx b2 ctx.lvl)
  | Pi(_,a,b), t1, t2 ->
    unify (Ctx.extend ctx) st (Eval.clAppVar ctx b ctx.lvl) (Eval.appVar ctx t1 a ctx.lvl) (Eval.appVar ctx t2 a ctx.lvl)

  (* Record *)
  | _, RecordType(sg1), RecordType(sg2) ->
    unify_record_sig ctx st (sg1,sg2)
  | RecordType(sg), t1, t2 ->
    unify_record ctx st (sg, Eval.record_eta_expand ctx sg t1, Eval.record_eta_expand ctx sg t2)

  (* Prop *)
  | _, StrPropType,StrPropType -> ()
  | _, Bottom, Bottom -> ()
  | _, StrPropCarrier(a), StrPropCarrier(b) ->
    unify ctx st StrPropType a b
  | StrPropCarrier(_), _, _ -> ()
  | _, StrProp(a), b ->
    unify ctx st Set a.carrier (Eval.str_prop_carrier ctx b)
  | _, a, StrProp(b) ->
    unify ctx st Set (Eval.str_prop_carrier ctx a) b.carrier

  (* Ring *)
  | _, StrRingType, StrRingType -> ()
  | _, StrRingCarrier(a), StrRingCarrier(b) ->
    unify ctx st Set a b
  | _, ZZ, ZZ -> ()
  | StrRingType, a, b ->
    unify ctx st (Eval.ringType ctx) (Eval.str_ring_to_ring ctx a) (Eval.str_ring_to_ring ctx b)
  | StrRingCarrier(a), v1, v2 ->
    unify_ring_poly ctx st a v1 v2

  (* OTT / SeTT *)
  | _, Eq(a1,x1,y1), Eq(a2,x2,y2) ->
    unify ctx UFlex Set a1 a2;
    unify ctx UFlex a1 x1 x2;
    unify ctx UFlex a1 y1 y2

  | _, Cast(a1,b1,_,x1), Cast(a2,b2,_,x2) ->
    unify ctx UFlex Set a1 a2;
    unify ctx UFlex a1 b1 b2;
    unify ctx UFlex a1 x1 x2

  (* Metas *)
  | _, Meta(m1,sp1), Meta(m2,sp2) when m1 = m2 ->
    intersect ctx st m1 sp1 sp2
  | _, (Meta(m1,sp1) as v1), (Meta(m2,sp2) as v2) when st <> UFlex ->
    solve_flex_flex ctx m1 sp1 v1 m2 sp2 v2
  | _, Meta(m1,sp1), v2 when st <> UFlex ->
    solve_meta ctx m1 sp1 v2
  | _, v1, Meta(m2,sp2) when st <> UFlex ->
    solve_meta ctx m2 sp2 v1

  (* Rigid *)
  | _, Var(x1,sp1), Var(x2,sp2) when x1 = x2 ->
    unify_spine ctx st (sp1,sp2)
  | _, Set,Set -> ()

  (* fail *)
  | _,_,_ -> raise UnifyError

and unify_spine ctx st = function
  | SEmpty,SEmpty ->
    ()
  | SApp(sp1,a1,v1), SApp(sp2,_,v2) ->
    unify_spine ctx st (sp1,sp2);
    unify ctx st a1 v1 v2
  | SProj(sp1,x1), SProj(sp2,x2) when x1 = x2 ->
    unify_spine ctx st (sp1,sp2)
  | _,_ -> raise UnifyError

and unify_record_sig ctx st = function
  | SigEmpty,SigEmpty -> ()
  | SigExtend(x,ty1,sg1),SigExtend(y,ty2,sg2) when x = y ->
    unify ctx st Set ty1 ty2;
    unify_record_sig (Ctx.extend ctx) st (Eval.sigClAppVar ctx sg1 ctx.lvl, Eval.sigClAppVar ctx sg2 ctx.lvl)
  | _,_ -> raise UnifyError

and unify_record ctx st = function
  | SigEmpty,RecEmpty,RecEmpty -> ()
  | SigExtend(_,ty,sg),RecExtend(_,a1,rc1),RecExtend(_,a2,rc2) ->
    unify ctx st ty a1 a2;
    unify_record ctx st (Eval.sigClApp ctx sg a1, rc1, rc2)
  | _,_,_ -> raise UnifyError

and unify_ring_poly ctx st a v1 v2 =
  let p : Value.poly = { ring = a;
                         vars = CCVector.of_list [v1;v2];
                         poly = ZPoly.generic_sub;
                       } in
  Eval.force_poly ctx p;
  if not (ZPoly.is_zero p.poly)
  then begin
    if st = UFlex then raise UnifyError;
    let sz = CCVector.size p.vars in
    let sols =
      ZPoly.solutions sz
        (CCVector.map (fun v -> Value.neutral_is_var (Eval.force ctx v)) p.vars)
        p.poly in
    let exception FoundSolution in
    try
      List.iter (fun (v,q) ->
          match Eval.force ctx (CCVector.get p.vars v) with
          | Meta(m,s) ->
            solve_meta ctx m s (Value.StrRingPoly { ring = a; vars = p.vars; poly = q; });
            raise FoundSolution
          | _ -> failwith ("impossible " ^ __LOC__)
        ) sols;
      raise UnifyError
    with FoundSolution -> ()
  end
