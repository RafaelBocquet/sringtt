open Common

(** The type of monomials with variables in ℕ. *)
(** A monomial is encoded as map from variables to positive exponents. *)
type monomial = int IntMap.t

module MonomialMap = Map.Make(struct
    type t = monomial
    let compare = IntMap.compare Stdlib.compare
  end)

(** The type of polynomials over ℤ with variables in ℕ. *)
(** A polynomial is encoded as a map from monomials to non-zero coefficients. *)
type t = Z.t MonomialMap.t

let pp fmt (p : t) =
  if MonomialMap.is_empty p
  then Format.fprintf fmt "0"
  else
    let first = ref true in
    MonomialMap.iter (fun mono c ->
        if not !first then begin
          Format.fprintf fmt " + ";
        end;
        first := false;
        Format.fprintf fmt "%s" (Z.to_string c);
        IntMap.iter (fun var exp ->
            Format.fprintf fmt " * X%d" var;
            if exp <> 1 then
              Format.fprintf fmt "^%d" exp;
          ) mono
      ) p

(** The zero polynomial. *)
let zero : t = MonomialMap.empty

(** Construct a constant polynomial. *)
let constant x : t = if Z.equal x Z.zero then zero else MonomialMap.singleton IntMap.empty x

(** Construct a variable polynomial. *)
let variable x = MonomialMap.singleton (IntMap.singleton x 1) Z.one

let some_if_nz x = if Z.equal Z.zero x then None else Some(x)

(** Test whether a polynomial is zero or not. *)
let is_zero (x : t) = MonomialMap.is_empty x

(** Addition of polynomials. *)
let add (x : t) (y : t) : t =
  MonomialMap.union (fun _ x y -> some_if_nz (Z.add x y)) x y

(** Negation of a polynomial. *)
let neg (x : t) : t =
  MonomialMap.map Z.neg x

(** Subtraction of polynomials. *)
let sub (x : t) (y : t) : t =
  add x (neg y)

(** Multiplication of polynomials. *)
let mul (x : t) (y : t) =
  let z = ref MonomialMap.empty in
  MonomialMap.iter (fun xm xc ->
      MonomialMap.iter (fun ym yc ->
          let zm = IntMap.union (fun _ a b -> Some (a+b)) xm ym in
          let zc = Z.mul xc yc in
          z := MonomialMap.update zm (function
              | None -> Some(zc)
              | Some(d) -> some_if_nz (Z.add zc d)
            ) !z
        ) y
    ) x;
  !z

(** Raise a polynomial to an integer power. *)
let rec power (x : t) = function
  | 0 -> constant Z.one
  | 1 -> x
  | n ->
    let y = power x (n/2) in
    if n mod 2 = 1
    then mul (mul x y) y
    else mul y y

(** N-ary composition of polynomials. *)
(** @param [x] is a polynomial. *)
(** @param [p] is map from the variables of [x] to polynomials. *)
let compose (p : t CCVector.vector) (x : t) =
  let ret = ref MonomialMap.empty in
  let go m c =
    ret := add !ret (IntMap.fold (fun x v -> mul (power (CCVector.get p x) v)) m (constant c))
  in
  MonomialMap.iter go x;
  !ret

(** Multiplication of a polynomial by a constant. *)
let mul_Z (p : t) z : t = MonomialMap.map (fun c -> Z.mul c z) p

(** Division of a polynomial by a constant. *)
(** Every coefficient of [p] should be divisible by [z]. *)
let div_Z (p : t) z : t = MonomialMap.map (fun c -> Z.div c z) p

(** {1 Generic operations} *)
(** For every operation of the algebraic theory of rings, there is a genetic polynomial obained by applying the operation to variables. *)

(** The generic polynomial for addition. *)
let generic_add : t =
  MonomialMap.of_seq
    (List.to_seq [(IntMap.singleton 0 1, Z.one); (IntMap.singleton 1 1, Z.one)])

let generic_sub : t =
  MonomialMap.of_seq
    (List.to_seq [(IntMap.singleton 0 1, Z.one); (IntMap.singleton 1 1, Z.minus_one)])

(** The generic polynomial for multiplication. *)
let generic_mul : t =
  MonomialMap.singleton (IntMap.of_seq (List.to_seq [(0,1); (1,1)])) Z.one

(** The generic polynomial for negation. *)
let generic_neg : t =
  MonomialMap.singleton (IntMap.singleton 0 1) Z.minus_one

(** Compose a polynomial [x] with a renaming [f]. *)
let map_variables f (p : t) =
  MonomialMap.of_seq (Seq.map (fun (y,c) ->
      (IntMap.of_seq (Seq.map (fun (z,e) -> (f z, e)) (IntMap.to_seq y)), c))
      (MonomialMap.to_seq p))

(** Return the support of a polynomial, i.e. the set of variables occuring in the polynomial. *)
let support (p : t) : IntSet.t =
  MonomialMap.fold (fun y _ -> IntSet.union (IntMap.fold (fun z _ -> IntSet.add z) y IntSet.empty))
    p IntSet.empty

(** Return the greatest common divisor of all coefficients of a polynomial. *)
let gcd_coeffs (p : t) : Z.t =
  let g = ref Z.zero in
  MonomialMap.iter (fun _ c -> g := Z.gcd !g c) p;
  !g

(** Return the greatest common divisor of all monomials occuring in a polynomial. *)
let gcd_mono (p : t) =
  let rec go = function
    | [] -> failwith "impossible"
    | [(m,_)] -> m
    | (m,_) :: ms ->
      IntMap.merge (fun _ a b -> match a,b with
          | Some(a),Some(b) -> Some(min a b)
          | _ -> None)
        m (go ms)
  in
  go (MonomialMap.bindings p)

(** Divides a polynomial by a monomial. *)
let div_mono (x : t) (m : monomial) =
  MonomialMap.of_seq (Seq.map (fun (y,c) ->
      (IntMap.merge (fun _ a b -> match a,b with
           | Some(a),None -> Some(a)
           | Some(a),Some(b) when a = b -> None
           | Some(a),Some(b) when a > b -> Some(a-b)
           | _ -> failwith "ZPoly.div_mono: impossible"
         ) y m, c))
      (MonomialMap.to_seq x))

(** Return the set of polynomial solutions of the equation [p = 0]. *)
(** A polynomial solution is a pair [(x,q)] such that substituting the polynomial [q] for the variable [x] in [p] yields the zero polynomial. *)
(** The polynomial [p] should be non-zero. *)
(** The parameter [rigid] specifies a subset of rigid variables, that are known to be cancellable. *)
let solutions (sz : int) (rigid : bool CCVector.vector) (p : t) : (int * t) list =
  (* 1. Divide p by the gcds of all coefficients and monomials (resticted to the rigid variables). *)
  let p = div_Z p (gcd_coeffs p) in
  let g = IntMap.filter (fun v _ -> CCVector.get rigid v) (gcd_mono p) in
  let p = div_mono p g in
  (* 2. Compute the number of occurences of every variable. *)
  (*    Variables that occur more than once are not solvable. *)
  let occur = CCVector.make sz 0 in
  MonomialMap.iter (fun m _ ->
        IntMap.iter (fun v e ->
          CCVector.set occur v (CCVector.get occur v + e)
        ) m
  ) p;
  (* 3. A variable is solvable if: *)
  (*    - It is not rigid. *)
  (*    - It only occurs once, *)
  (*    - in a monomial of degree 1, *)
  (*    - and its coefficient is invertible (1 or -1 in ℤ). *)
  let sols = ref [] in
  MonomialMap.iter (fun m c ->
      if IntMap.cardinal m = 1 && Z.equal (Z.abs c) Z.one then
        IntMap.iter (fun v e ->
            if not (CCVector.get rigid v) && e = 1 && CCVector.get occur v = 1 then
              sols := (v, neg (sub (div_Z p c) (variable v))) :: !sols
        ) m
  ) p;
  !sols

(* type repr = Var of int *)
(*           | Zero *)
(*           | One *)
(*           | Neg of repr *)
(*           | Add of repr * repr *)
(*           | Mul of repr * repr *)

(* let to_repr (x : t) : repr = *)
(*   let rec positive_constant z = *)
(*     if Z.equal z Z.one then One *)
(*     else *)
(*       let half = Z.div z (Z.of_int 2) in *)
(*       let r = positive_constant half in *)
(*       if Z.equal (Z.(mod) z (Z.of_int 2)) Z.zero *)
(*       then Add(r,r) *)
(*       else Add(Add(r,r),One) *)
(*   and constant z = *)
(*     if Z.equal z Z.zero then Zero *)
(*     else if Z.lt z Z.zero then Neg (positive_constant (Z.neg z)) *)
(*     else positive_constant z *)
(*   and go_mono = function *)
(*     | [] -> One *)
(*     | [(x,e)] -> power (Var(x)) e *)
(*     | (x,e)::xs -> Mul(power (Var(x)) e, go_mono xs) *)
(*   and power x = function *)
(*     | 1 -> x *)
(*     | n -> Mul(x,power x (n-1)) *)
(*   and go_mono_coeff m c = *)
(*     if IntMap.is_empty m then constant c *)
(*     else *)
(*       let m' = go_mono (List.of_seq (IntMap.to_seq m)) in *)
(*       if Z.equal c Z.one then m' *)
(*       else Mul(constant c,m') *)
(*   and go = function *)
(*     | [] -> Zero *)
(*     | [(m,c)] when IntMap.is_empty m -> constant c *)
(*     | [(m,c)] -> go_mono_coeff m c *)
(*     | (m,c)::xs -> Add(go_mono_coeff m c, go xs) *)
(*   in *)
(*   go (List.of_seq (MonomialMap.to_seq x)) *)
