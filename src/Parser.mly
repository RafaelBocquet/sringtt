
%{

    %}


%token EOF

%token LPAREN "("
%token RPAREN ")"
%token LBRACE "{"
%token RBRACE "}"
%token LBRACK "["
%token RBRACK "]"
%token BACKSLASH "\\"
%token UNDERSCORE "_"
%token EQUAL "="
%token COMMA ","
%token COLON ":"
%token SEMICOLON ";"
%token DOT "."
%token RARROW "->"
%token BOTTOM

%token DEF
%token LET
%token SET
%token RECORD_TYPE
%token RECORD
%token STRPROP_TYPE
%token PROP
%token ABSURD
%token STRRING_TYPE
%token RING
%token ZZ
%token EQ
%token REFL
%token CAST

%token <string> PROJ
%token <string> IDENT
%token <string> OPERATOR
%token <Z.t> INTLIT

%start <Raw.top list> file

%%

file:
  | xs=list(terminated(top, ";")); EOF { xs }

top:
  | DEF; x=IDENT; "="; t=tm; { Raw.Def(x,None,t) }
  | DEF; x=IDENT; ":"; a=tm; "="; t=tm; { Raw.Def(x,Some(a),t) }

(* *)

tm_atom:
  | "("; x=tm; ")" { x }
  | x=IDENT { Raw.Var(x) }
  | x=INTLIT { Raw.Intlit(x) }
  | "_" { Raw.Implicit }
  | SET { Raw.Set }
  | t=tm_record_type { t }
  | t=tm_record { t }

  | STRPROP_TYPE { Raw.StrPropType }
  | PROP { Raw.StrProp }
  | BOTTOM { Raw.Bottom }
  | ABSURD { Raw.Absurd }

  | STRRING_TYPE { Raw.StrRingType }
  | RING { Raw.StrRing }
  | ZZ { Raw.ZZ }

  | EQ { Raw.Eq }
  | REFL { Raw.Refl }
  | CAST { Raw.Cast }

tm_spine_item:
  | x=tm_atom { fun a -> Raw.App(Expl,a,x) }
  | x=PROJ { fun a -> Raw.Proj(a,x) }
  | "{"; x=tm; "}" { fun a -> Raw.App(Impl,a,x) }

tm_spine:
  | ys=list(tm_spine_item) { ys }

tm_apps:
  | x=tm_atom; ys=tm_spine { List.fold_left (fun a b -> b a) x ys }

tm_apps_then_binder:
  | x=tm_apps { x }
  | x=tm_apps; z=tm_binder { Raw.App(Expl,x,z) }

tm_arrs:
  | x=tm_apps_then_binder { x }
  | x=tm_apps; "->"; y=tm_arrs { Raw.Pi((Expl,None,Some(x)), y) }
  | xs=nonempty_list(binder_full); "->"; y=tm_arrs { List.fold_right (fun a b -> Raw.Pi(a,b)) xs y }

tm_let:
  | LET; x=IDENT; "="; t=tm; ";"; u=tm { Raw.Let(x,None,t,u) }
  | LET; x=IDENT; ":"; a=tm; "="; t=tm; ";"; u=tm { Raw.Let(x,Some(a),t,u) }

binder:
  | x=ident_opt { (Expl,x,None) }
  | x=binder_full { x }

binder_full:
  | "("; x=ident_opt; ":"; a=tm ")" { (Expl,x,Some(a)) }
  | "{"; x=ident_opt; ":"; a=tm "}" { (Impl,x,Some(a)) }
  | "{"; x=ident_opt; "}" { (Impl,x,None) }

tm_lambda:
  | "\\"; xs=list(binder); "."; u=tm
    { List.fold_right (fun a b -> Raw.Lam(a,b)) xs u }

/* tm_forall: */
/* | FORALL; xs=list(binder); "."; u=tm */
/*     { List.fold_right (fun a b -> Raw.Pi(a,b)) xs u } */

tm_binder:
  | x=tm_lambda { x }
/* | x=tm_forall { x } */

record_type_field:
  | x=IDENT; ":"; a=tm { (x,a) }

tm_record_type:
  | RECORD_TYPE; "{"; xs=list(terminated(record_type_field, ";")) ;"}"
    { Raw.RecordType(List.fold_right (fun (x,a) s -> Raw.SigExtend(x,a,s)) xs Raw.SigEmpty) }

record_field:
  | x=IDENT; "="; a=tm { (x,a) }

tm_record:
  | RECORD; "{"; xs=list(terminated(record_field, ";")) ;"}"
    { Raw.Record(List.fold_right (fun (x,a) s -> Raw.RecExtend(x,a,s)) xs Raw.RecEmpty) }

tm:
  | x=tm_binder { x }
  | x=tm_let { x }
  | x=tm_arrs { x }

ident_opt:
  | x=IDENT { Some(x) }
  | "_" { None }
