open Common

type tm
  = Var of string
  | Let of string * tm option * tm * tm
  | Implicit
  | Set
  (* Pi *)
  | Pi of binder * tm
  | Lam of binder * tm
  | App of icit * tm * tm
  (* Record *)
  | RecordType of record_sig
  | Record of record
  | Proj of tm * string
  (* StrProp *)
  | StrPropType
  | StrProp
  | Bottom
  | Absurd
  (* StrRing *)
  | StrRingType
  | StrRing
  | StrRingAdd of tm * tm * tm
  | StrRingNeg of tm * tm
  | StrRingMul of tm * tm * tm
  | ZZ
  | Intlit of Z.t [@printer fun fmt x -> fprintf fmt "%s" (Z.to_string x)]
  (* OTT / SeTT *)
  | Eq
  | Refl
  | Cast
[@@deriving show]

and record_sig =
  | SigEmpty
  | SigExtend of string * tm * record_sig
[@@deriving show]

and record =
  | RecEmpty
  | RecExtend of string * tm * record
[@@deriving show]


and binder = icit * string option * tm option
[@@deriving show]

type top
  = Def of string * tm option * tm
[@@deriving show]

type file = top list
[@@deriving show]

