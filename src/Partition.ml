open Common

type t = { mutable sz : int;
           array : int CCVector.vector;
           repr  : int CCVector.vector;
         }

let create () = { sz    = 0;
                  array = CCVector.create ();
                  repr  = CCVector.create ();
                }

let push (p : t) (x : int option) = match x with
  | Some(x) ->
    CCVector.push p.array x
  | None ->
    let x = p.sz in
    p.sz <- p.sz+1;
    CCVector.push p.repr (CCVector.size p.array);
    CCVector.push p.array x
