open Common

type t
  = Var of lvl * spine
  | Meta of meta * spine
  | Set

  | Pi of icit * t * tm_closure
  | Lam of tm_closure

  | RecordType of record_sig
  | Record of record

  | StrPropType
  | StrProp of str_prop
  | StrPropCarrier of t
  | Bottom

  | StrRingType
  | StrRing of str_ring
  | StrRingCarrier of t
  | StrRingPoly of poly
  | ZZ

  | Eq of t * t * t
  | Refl of t * t
  | Cast of t * t * t * t

  | Sym of t
  | Trans of t * t
[@@deriving show]

and env = EEmpty
        | ESnoc of env * t
[@@deriving show]

and spine = SEmpty
          | SApp of spine * (* type *) t * (* arg *) t
          | SProj of spine * string
          | SAbsurd of spine
[@@deriving show]

and record_sig = SigEmpty
               | SigExtend of string * t * sig_closure
[@@deriving show]

and record = RecEmpty
           | RecExtend of string * t * record
[@@deriving show]

and str_prop = { carrier : t;
                 mutable isProp : t option;
                 mutable is_strict : bool;
               }
[@@deriving show]

and str_ring = { carrier : t;
                 zero    : t;
                 one     : t;
                 add     : t;
                 neg     : t;
                 mul     : t;
                 mutable add_assoc   : t option;
                 mutable add_comm    : t option;
                 mutable add_neutral : t option;
                 mutable add_inverse : t option;
                 mutable mul_assoc   : t option;
                 mutable mul_comm    : t option;
                 mutable mul_neutral : t option;
                 mutable mul_distr   : t option;
                 mutable is_strict : bool;
               }
[@@deriving show]

and poly = { ring : t;
             mutable vars : t CCVector.vector;
             [@printer Util.pp_vector pp]
             mutable poly : ZPoly.t;
           }
[@@deriving show]

and ('a,'b) closure = Cl of env * 'a
                    | ClFun of (t -> 'b)
                    | ClConst of 'b
[@@deriving show]

and tm_closure = (Term.t, t) closure
and sig_closure = (Term.record_sig, record_sig) closure

let env_lookup env (Idx(n) : idx) =
  let rec go env i = match env, i with
  | EEmpty, _       -> failwith ("impossible " ^ __LOC__)
  | ESnoc(_  ,a), 0 -> a
  | ESnoc(env,_), n -> go env (n-1)
  in go env n

let neutral_is_var = function
  | Var(_) -> true
  | Meta(_) -> false
  | _ -> failwith ("impossible " ^ __LOC__)

let var x = Var(x,SEmpty)
let meta x = Meta(x,SEmpty)
let lam f = Lam(ClFun(f))

let record xs =
  let rec go : (string * t) list -> record = function
    | [] -> RecEmpty
    | (x,v)::xs -> RecExtend(x,v,go xs)
  in Record(go xs)

(** Restricts a polynomial to its support. *)
let restrict_ring_poly (p : poly) =
  let supp = ZPoly.support p.poly in
  if IntSet.cardinal (ZPoly.support p.poly) <> CCVector.size p.vars then begin
    let vars = CCVector.create () in
    let mp    = CCVector.make (CCVector.size p.vars) (-1) in
    IntSet.iter (fun i ->
        CCVector.set mp i (CCVector.size vars);
        CCVector.push vars (CCVector.get p.vars i)
      ) supp;
    p.vars <- vars;
    p.poly <- ZPoly.map_variables (CCVector.get mp) p.poly
  end
