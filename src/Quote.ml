open Common

module Ctx = EvalCtx

let rec quote ectx v = quote_ ectx (Eval.force ectx v)
and quote_ ectx : Value.t -> Term.t = function
  | Var(v,s) ->
    quote_spine ectx (Term.Var(lvl_to_idx ectx.lvl v)) s
  | Meta(m,s) ->
    quote_spine ectx (Term.Meta(m)) s
  | Set -> Set
  | Pi(pl,a,b) ->
    Term.Pi(pl,
            quote ectx a,
            quote (Ctx.extend ectx) (Eval.clAppVar (Ctx.extend ectx) b ectx.lvl))
  | Lam(b) ->
    Term.Lam(quote (Ctx.extend ectx) (Eval.clAppVar (Ctx.extend ectx) b ectx.lvl))
  | RecordType(sg) ->
    RecordType(quote_record_sig ectx sg)
  | Record(rc) ->
    Record(quote_record ectx rc)
  (* StrProp *)
  | StrPropType ->
    StrPropType
  | StrProp(a) ->
    let carrier = a.carrier in
    let isProp  = match a.isProp with
      | None -> Value.(lam (fun x -> lam (fun _ -> Refl(carrier,x))))
      | Some(x) -> x
    in
    StrProp(Record(RecExtend("type", quote ectx carrier,
                             RecExtend("isProp", quote ectx isProp,
                                       RecEmpty))))
  | StrPropCarrier(a) ->
    StrPropCarrier(quote ectx a)
  | Bottom ->
    Bottom
  (* StrRing *)
  | StrRingType ->
    StrRingType
  | StrRingCarrier(a) ->
    StrRingCarrier(quote ectx a)
  | StrRingPoly(p) ->
    StrRingPoly({ ring = quote ectx p.ring;
                  vars = CCVector.map (quote ectx) p.vars;
                  poly = p.poly;
                })
  | ZZ ->
    ZZ
  (* OTT/SeTT *)
  | Eq(a,x,y) -> Eq(quote ectx a, quote ectx x, quote ectx y)
  | Refl(a,x) -> Refl(quote ectx a, quote ectx x)

  | v ->
    Format.fprintf Format.std_formatter "%a@." Value.pp v;
    failwith __LOC__

and quote_spine ectx acc : Value.spine -> _ = function
  | SEmpty       -> acc
  | SApp(s,ty,v) -> Term.App(quote_spine ectx acc s, quote ectx ty, quote ectx v)
  | SProj(s,x)   -> Term.Proj(quote_spine ectx acc s, x)
  | SAbsurd(s)   -> Term.Absurd(quote_spine ectx acc s)

and quote_record_sig ectx = function
  | SigEmpty ->
    SigEmpty
  | SigExtend(x,ty,sg) ->
    SigExtend(x,
              quote ectx ty,
              quote_record_sig (Ctx.extend ectx) (Eval.sigClAppVar (Ctx.extend ectx) sg ectx.lvl))

and quote_record ectx = function
  | RecEmpty ->
    RecEmpty
  | RecExtend(x,a,rc) ->
    RecExtend(x,
              quote ectx a,
              quote_record ectx rc)
