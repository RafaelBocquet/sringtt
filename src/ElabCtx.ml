open Common

type name_data
  = Local of lvl
  | Global of top

type ctx = { gctx  : Global.ctx;

             names : name_data NameMap.t;
             lvl   : lvl;
             env   : Value.env;
             types : Value.env;

             bindings : (lvl * Term.t * Value.t) list;
           }

let empty gctx globals : ctx
  = { gctx  = gctx;

      names = globals;
      lvl   = 0;
      env   = Value.EEmpty;
      types = Value.EEmpty;

      bindings = [];
    }

let get_ectx (ctx : ctx) : EvalCtx.ctx
  = EvalCtx.create ctx.gctx ctx.lvl

let get_ectx_closed (ctx : ctx) : EvalCtx.ctx
  = EvalCtx.create ctx.gctx 0

let bind (ctx : ctx) x vty : ctx
  = { gctx  = ctx.gctx;
      names = begin match x with
        | Some x -> NameMap.add x (Local ctx.lvl) ctx.names
        | None -> ctx.names
      end;
      lvl   = ctx.lvl+1;
      env   = Value.ESnoc(ctx.env, Value.var ctx.lvl);
      types = Value.ESnoc(ctx.types, vty);

      bindings = (ctx.lvl, Quote.quote (get_ectx ctx) vty, vty) :: ctx.bindings;
    }

let define (ctx : ctx) x vty vu : ctx
  = { gctx  = ctx.gctx;
      names = begin match x with
        | Some x -> NameMap.add x (Local ctx.lvl) ctx.names
        | None -> ctx.names
      end;
      lvl   = ctx.lvl+1;
      env   = Value.ESnoc(ctx.env, vu);
      types = Value.ESnoc(ctx.types, vty);

      bindings = ctx.bindings;
    }

let lookup ctx x : Term.t * Value.t
  = try match NameMap.find x ctx.names with
  | Local(lvl) ->
    let ix = lvl_to_idx ctx.lvl lvl in
    Term.Var(ix), Value.env_lookup ctx.types ix
  | Global(i) -> Term.Top(i), (Top.lookup ctx.gctx.tctx i).vty
  with
  | Not_found -> failwith ("Not_found " ^ x)

let force ctx = Eval.force (get_ectx ctx)
let eval ctx = Eval.eval (get_ectx ctx) ctx.env
let quote ctx = Quote.quote (get_ectx ctx)

let clApp ctx = Eval.clApp (get_ectx ctx)
let clAppVar ctx = Eval.clAppVar (get_ectx ctx)
let sigClApp ctx = Eval.sigClApp (get_ectx ctx)
let sigClAppVar ctx = Eval.sigClAppVar (get_ectx ctx)
let proj_type ctx = Eval.proj_type (get_ectx ctx)


let close_ty ctx ty =
  let tm = Quote.quote (get_ectx ctx) ty in
  let rec go acc = function
    | []      -> acc
    | (_,a,_) :: bs -> go (Term.Pi(Expl,a,acc)) bs
  in go tm ctx.bindings

let fresh_meta ctx ty =
  let m = Meta.fresh_meta ctx.gctx.mctx (Eval.eval (get_ectx_closed ctx) EEmpty (close_ty ctx ty)) in
  let t = List.fold_right (fun (x,ty,_) t -> Term.App(t,ty,Term.Var(lvl_to_idx ctx.lvl x)))
      ctx.bindings (Term.Meta(m)) in
  let s = List.fold_right (fun (x,_,vty) s -> Value.SApp(s,vty,Value.var x))
      ctx.bindings Value.SEmpty in
  let v = Value.Meta(m,s) in
  t, v

let unify ctx s a b =
  try Unify.unify (get_ectx ctx) URigid s a b
  with Unify.UnifyError ->
    Format.fprintf Format.std_formatter "Can't unify\n  %a@.with\n  %a@.in type\n  %a@."
      Term.pp (quote ctx a)
      Term.pp (quote ctx b)
      Term.pp (quote ctx s);
    failwith "UnifyError"
