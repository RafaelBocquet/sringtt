type idx = Idx of int
[@@unboxed]
[@@deriving show]

type lvl = int
[@@deriving show]

type meta = Meta of int
[@@unboxed]

let pp_meta fmt (Meta(m)) = Format.fprintf fmt "?%d" m

type top = Top of int
[@@unboxed]
[@@deriving show]

type icit
  = Impl
  | Expl
[@@deriving show]

module IntSet = Set.Make(Int)
module IntMap = Map.Make(Int)
module NameMap = Map.Make(String)
module MetaSet = Set.Make(struct
    type t = meta
    let compare = Stdlib.compare
  end)

let lvl_to_idx (sz : lvl) (lvl : lvl) : idx
  = Idx (sz-1-lvl)

type unify_state = URigid | UFlex
[@@deriving show]
