open Common

module Ctx = ElabCtx

let rec insertImplicit ctx (tm, vty) = match Ctx.force ctx vty with
  | Pi(Impl,va,b) ->
    let m,vm = Ctx.fresh_meta ctx va in
    let a = Ctx.quote ctx va in
    insertImplicit ctx (Term.App(tm,a,m), Ctx.clApp ctx b vm)
  | _ -> (tm,vty)

let rec check (ctx : Ctx.ctx) (ty : Value.t) (rtm : Raw.tm) : Term.t =
  check_ ctx (rtm,ty)

and check_ ctx = function
  | (Lam((pl,x,None),rt), Pi(pl',a,b)) when pl = pl' ->
    let t = check (Ctx.bind ctx x a) (Ctx.clAppVar ctx b ctx.lvl) rt in
    Term.Lam(t)

  | (rtm, Pi(Impl,a,b)) ->
    let t = check (Ctx.bind ctx None a) (Ctx.clAppVar ctx b ctx.lvl) rtm in
    Term.Lam(t)

  | (Implicit, ty) ->
    let u, _ = Ctx.fresh_meta ctx ty in
    u

  | (Let(x,ra_opt,rt,ru),ty) ->
    let va = match ra_opt with
      | Some(ra) ->
        let a  = check ctx Set ra in
        Ctx.eval ctx a
      | None ->
        let _,va = Ctx.fresh_meta ctx Set
        in va
    in
    let t  = check ctx va rt in
    let vt = Ctx.eval ctx t in
    let u  = check (Ctx.define ctx (Some(x)) va vt) ty ru in
    Term.Let(t,u)

  | (Record(rrc), RecordType(sg)) ->
    let rc = check_record ctx (sg,rrc) in
    Term.Record(rc)

  | rtm,ty -> check_default ctx ty rtm

and check_default ctx ty rtm =
  let tm, ty' = insertImplicit ctx (infer ctx rtm) in
  Ctx.unify ctx Set ty ty';
  tm

and check_type ctx rtm =
  let tm, ty' = infer ctx rtm in
  Ctx.unify ctx Set Set ty';
  tm

and check_record ctx : Value.record_sig * Raw.record -> Term.record = function
  | (SigEmpty, RecEmpty) -> RecEmpty
  | (SigExtend(x,ty,sg), RecExtend(x',ra,rrc)) when x = x' ->
    let a  = check ctx ty ra in
    let va = Ctx.eval ctx a in
    let rc = check_record ctx (Ctx.sigClApp ctx sg va, rrc) in
    RecExtend(x,a,rc)
  | (_,_) -> failwith "check_record: fail"


and check_record_sig ctx : Raw.record_sig -> Term.record_sig = function
  | SigEmpty ->
    SigEmpty
  | SigExtend(x,rty,rsg) ->
    let ty  = check_type ctx rty in
    let vty = Ctx.eval ctx ty in
    let sg  = check_record_sig (Ctx.bind ctx (Some(x)) vty) rsg in
    SigExtend(x,ty,sg)

and infer (ctx : Ctx.ctx) : Raw.tm -> Term.t * Value.t = function
  | Var(x)
    -> Ctx.lookup ctx x

  | Set ->
    Term.Set, Value.Set

  | Implicit ->
    let _, va = Ctx.fresh_meta ctx Set in
    let u, _  = Ctx.fresh_meta ctx va in
    u, va

  | Let(x,ra_opt,rt,ru) ->
    let va = match ra_opt with
      | Some(ra) ->
        let a  = check ctx Set ra in
        Ctx.eval ctx a
      | None ->
        let _,va = Ctx.fresh_meta ctx Set
        in va
    in
    let t  = check ctx va rt in
    let vt = Ctx.eval ctx t in
    let u, ty = infer (Ctx.define ctx (Some(x)) va vt) ru in
    Term.Let(t,u), ty

  | Pi((pl,x,ra_opt),rb) ->
    let a,va = match ra_opt with
      | Some ra ->
        let a = check_type ctx ra in
        a, Ctx.eval ctx a
      | None -> Ctx.fresh_meta ctx Set
    in
    let b = check_type (Ctx.bind ctx x va) rb in
    Term.Pi(pl,a,b), Set

  | Lam((pl,x,ra),rt) ->
    let va = match ra with
      | Some(ra) ->
        let a = check_type ctx ra in
        Ctx.eval ctx a
      | None ->
        let _, vs = Ctx.fresh_meta ctx Set in
        let _, va = Ctx.fresh_meta ctx vs in
        va
    in
    let ctxb  = Ctx.bind ctx x va in
    let t, vb = infer ctxb rt in
    let b     = Ctx.quote ctxb vb in
    Term.Lam(t), Value.Pi(pl,va, Cl(ctx.env, b))

  | App(pl,rf,ru) ->
    let f, ty = match pl with
      | Impl -> infer ctx rf
      | Expl -> insertImplicit ctx (infer ctx rf)
    in
    let (va,b) = match Ctx.force ctx ty with
      | Pi(pl',va,b) when pl = pl' -> va,b
      | _ ->
        let _, va = Ctx.fresh_meta ctx Set in
        let b, _  = Ctx.fresh_meta (Ctx.bind ctx None va) Set in
        let b     = Value.Cl(ctx.env,b) in
        Ctx.unify ctx Set ty (Value.Pi(pl,va,b));
        va, b
    in
    let u = check ctx va ru in
    let a = Ctx.quote ctx va in
    Term.App(f,a,u), Ctx.clApp ctx b (Ctx.eval ctx u)

  | RecordType(rsg) ->
    let sg = check_record_sig ctx rsg in
    Term.RecordType(sg), Value.Set

  | Record(RecEmpty) ->
    Term.Record(RecEmpty), Value.RecordType(SigEmpty)

  | Record(_) ->
    failwith "cannot infer record terms"

  | Proj(rt,x) ->
    let t,ty = insertImplicit ctx (infer ctx rt) in
    begin match Ctx.force ctx ty,x with
      | RecordType(sg),x ->
        let vt = Ctx.eval ctx t in
        begin match Ctx.proj_type ctx sg vt x with
          | None ->
            failwith ("unknown field " ^ x)
          | Some(va) ->
            Proj(t,x), va
        end
      | StrPropType,"type" -> StrPropCarrier(t), Set
      | StrRingType,"type" -> StrRingCarrier(t), Set
      | StrRingType,"zero" ->
        let vt = Ctx.eval ctx t in
        let carrier = Eval.ring_carrier (Ctx.get_ectx ctx) vt in
        Term.StrRingZero(t),
        carrier
      | StrRingType,"one" ->
        let vt = Ctx.eval ctx t in
        let carrier = Eval.ring_carrier (Ctx.get_ectx ctx) vt in
        Term.StrRingOne(t),
        carrier
      | StrRingType,"add" ->
        let vt = Ctx.eval ctx t in
        let carrier = Eval.ring_carrier (Ctx.get_ectx ctx) vt in
        Term.StrRingAdd(t),
        Value.Pi(Expl, carrier, ClFun(fun _ ->
            Value.Pi(Expl, carrier, ClFun(fun _ ->
                carrier))))
      | StrRingType,"neg" ->
        let vt = Ctx.eval ctx t in
        let carrier = Eval.ring_carrier (Ctx.get_ectx ctx) vt in
        Term.StrRingNeg(t),
        Value.Pi(Expl, carrier, ClFun(fun _ ->
                carrier))
      | StrRingType,"mul" ->
        let vt = Ctx.eval ctx t in
        let carrier = Eval.ring_carrier (Ctx.get_ectx ctx) vt in
        Term.StrRingMul(t),
        Value.Pi(Expl, carrier, ClFun(fun _ ->
            Value.Pi(Expl, carrier, ClFun(fun _ ->
                carrier))))
      | _ -> failwith ("unknown field " ^ x)
    end

  | StrPropType ->
    Term.StrPropType, Value.Set

  | StrProp ->
    Term.Lam(Term.StrProp(Term.Var(Idx(0)))),
    Value.Pi(Expl, Eval.hPropType (Ctx.get_ectx ctx), ClConst(Value.StrPropType))

  | Bottom ->
    Term.Bottom, Value.StrPropType

  | Absurd ->
    Term.(Lam(Lam(Absurd(Var(Idx(1)))))),
    Ctx.eval ctx
      Term.(Pi(Expl, StrPropCarrier(Bottom),
              Pi(Expl, Set,
                Var(Idx(0)))))

  | StrRingType ->
    Term.StrRingType, Value.Set

  | StrRing ->
    Term.Lam(Term.StrRing(Term.Var(Idx(0)))),
    Value.Pi(Expl, Eval.ringType (Ctx.get_ectx ctx), ClConst(Value.StrRingType))

  | ZZ ->
    Term.ZZ, Value.StrRingType

  | Intlit(z) ->
    Term.StrRingConstant(Term.ZZ, z),
    Value.StrRingCarrier(Value.ZZ)

  | Eq ->
    Term.(Lam(Lam(Lam(Eq(Var(Idx(2)),Var(Idx(1)),Var(Idx(0))))))),
    Ctx.eval ctx
      Term.(Pi(Expl, Set,
              Pi(Expl, Var(Idx(0)),
                 Pi(Expl, Var(Idx(1)),
                    StrPropType))))

  | Refl ->
    Term.(Lam(Lam(Refl(Var(Idx(1)),Var(Idx(0)))))),
    Ctx.eval ctx
      Term.(Pi(Expl, Set,
              Pi(Expl, Var(Idx(0)),
                 StrPropCarrier(Eq(Var(Idx(1)),Var(Idx(0)),Var(Idx(0)))))))

  | Cast ->
    Term.(Lam(Lam(Lam(Lam(Cast(Var(Idx(3)), Var(Idx(2)),Var(Idx(1)),Var(Idx(0)))))))),
    Ctx.eval ctx
      Term.(Pi(Expl, Set,
              Pi(Expl, Set,
                 Pi(Expl, StrPropCarrier(Eq(Set,Var(Idx(1)),Var(Idx(0)))),
                    Pi(Expl, Var(Idx(2)),
                       Var(Idx(2)))))))


  | v ->
    Format.fprintf Format.std_formatter "TODO: %a\n" Raw.pp_tm v;
    failwith __LOC__

let elab_top (gctx : Global.ctx) globals : Raw.top -> unit = function
  | Def(x,rty_opt,rtm) ->
    let ctx  = Ctx.empty gctx !globals in
    let ectx = EvalCtx.create gctx 0 in
    let ty,vty,tm,vtm = match rty_opt with
      | Some(rty) ->
        let ty   = check_type ctx rty in
        let vty  = Eval.eval ectx EEmpty ty in
        let tm   = check ctx vty rtm in
        let vtm  = Eval.eval ectx EEmpty tm in
        ty,vty,tm,vtm
      | None ->
        let tm, vty = infer ctx rtm in
        let vtm = Eval.eval ectx EEmpty tm in
        let ty = Quote.quote ectx vty in
        ty,vty,tm,vtm
    in
    let i    = Top.push gctx.tctx ty vty tm vtm x in
    globals := NameMap.add x (Ctx.Global(i)) !globals;
    ()

let elab_file (f : Raw.file) =
  let gctx = Global.create_ctx () in
  let globals = ref NameMap.empty in
  List.iter (elab_top gctx globals) f
