open Common

type ctx = { gctx : Global.ctx;
             lvl  : int;
           }

let create gctx lvl : ctx
  = { gctx = gctx;
      lvl  = lvl;
    }

let extend (ectx : ctx) = { ectx with lvl = ectx.lvl+1 }
let extend_n n (ectx : ctx) =
  assert(n >= 0);
  { ectx with lvl = ectx.lvl+n }
