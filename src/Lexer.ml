
open Sedlexing
open Parser

let digit  = [%sedlex.regexp? '0' .. '9']
let int    = [%sedlex.regexp? Star digit]
let ident  = [%sedlex.regexp? id_start, (Star id_continue)]

let rec token lb =
  match%sedlex lb with
  | eof -> EOF
  | white_space -> token lb
  | "--", Star (Compl ('\n' | '\r')) -> token lb

  | '(' -> LPAREN
  | ')' -> RPAREN
  | '{' -> LBRACE
  | '}' -> RBRACE
  | '[' -> LBRACK
  | ']' -> RBRACK
  | '\\' -> BACKSLASH
  | '_' -> UNDERSCORE
  | '=' -> EQUAL
  | ',' -> COMMA
  | ':' -> COLON
  | ';' -> SEMICOLON
  | '.' -> DOT
  | "->" -> RARROW
  | "_|_" -> BOTTOM

  | "def"  -> DEF
  | "let"  -> LET
  | "Set"  -> SET

  | "Record" -> RECORD_TYPE
  | "record" -> RECORD

  | "StrProp" -> STRPROP_TYPE
  | "prop" -> PROP
  | "absurd" -> ABSURD

  | "StrRing" -> STRRING_TYPE
  | "ring" -> RING
  | "ZZ" -> ZZ

  | "Eq" -> EQ
  | "refl" -> REFL
  | "case" -> CAST

  | ".", ident -> PROJ (Utf8.sub_lexeme lb 1 (lexeme_length lb - 1))
  | ident -> IDENT (Utf8.lexeme lb)
  | int -> INTLIT (Z.of_string (Latin1.lexeme lb))

  | _ -> failwith __LOC__
