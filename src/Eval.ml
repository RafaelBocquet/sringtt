open Common
module Ctx = EvalCtx

exception ConvError

let rec force (ctx : Ctx.ctx) : Value.t -> Value.t = function
  | Meta(m,sp) -> begin match Meta.lookup ctx.gctx.mctx m with
      | Unsolved(_)   -> Meta(m,sp)
      | Solved(_,_,v) -> force ctx (appSp ctx v sp)
    end
  (* Prop *)
  | StrProp(a) -> force_str_prop ctx a; StrProp(a)
  | StrPropCarrier(t) -> str_prop_carrier ctx t
  (* Ring *)
  | StrRing(a) -> force_str_ring ctx a; StrRing(a)
  | StrRingCarrier(t) -> ring_carrier ctx t
  | StrRingPoly(poly) -> force_poly ctx poly; StrRingPoly(poly)
  (* OTT/SeTT *)
  | Eq(a,x,y) -> eq ctx a x y
  | Cast(a,b,e,x) -> cast ctx a b e x
  (* . *)
  | v -> v

and force_str_prop ctx (a : Value.str_prop) =
  a.is_strict <- true;
  if Option.is_some a.isProp then
     if conv_bool (Ctx.extend_n 2 ctx) a.carrier (Value.var ctx.lvl) (Value.var (ctx.lvl+1))
     then a.isProp <- None
     else a.is_strict <- false;
  ()

and force_str_ring ctx (a : Value.str_ring) =
  let ty   = a.carrier in
  let zero = a.zero in
  let one  = a.one in
  let add  = a.add in
  let neg  = a.neg in
  let mul  = a.mul in
  let addf x y = app ctx (app ctx add ty x) ty y in
  let negf x   = app ctx neg ty x in
  let mulf x y = app ctx (app ctx mul ty x) ty y in
  a.is_strict <- true;
  let x0 = Value.var ctx.lvl in
  let x1 = Value.var (ctx.lvl+1) in
  let x2 = Value.var (ctx.lvl+2) in

  if Option.is_some a.add_assoc then
    if conv_bool (Ctx.extend_n 3 ctx) a.carrier (addf x0 (addf x1 x2)) (addf (addf x0 x1) x2)
    then a.add_assoc <- None
    else a.is_strict <- false;

  if Option.is_some a.add_comm then
    if conv_bool (Ctx.extend_n 2 ctx) a.carrier (addf x0 x1) (addf x1 x0)
    then a.add_comm <- None
    else a.is_strict <- false;

  if Option.is_some a.add_neutral then
    if conv_bool (Ctx.extend_n 1 ctx) a.carrier (addf x0 zero) x0
    then a.add_neutral <- None
    else a.is_strict <- false;

  if Option.is_some a.add_inverse then
    if conv_bool (Ctx.extend_n 1 ctx) a.carrier (addf x0 (negf x0)) zero
    then a.add_inverse <- None
    else a.is_strict <- false;

  if Option.is_some a.mul_assoc then
    if conv_bool (Ctx.extend_n 3 ctx) a.carrier (mulf x0 (mulf x1 x2)) (mulf (mulf x0 x1) x2)
    then a.mul_assoc <- None
    else a.is_strict <- false;

  if Option.is_some a.mul_comm then
    if conv_bool (Ctx.extend_n 2 ctx) a.carrier (mulf x0 x1) (mulf x1 x0)
    then a.mul_comm <- None
    else a.is_strict <- false;

  if Option.is_some a.mul_neutral then
    if conv_bool (Ctx.extend_n 1 ctx) a.carrier (mulf x0 one) x0
    then a.mul_neutral <- None
    else a.is_strict <- false;

  if Option.is_some a.mul_distr then
    if conv_bool (Ctx.extend_n 3 ctx) a.carrier (mulf x0 (addf x1 x2)) (addf (mulf x0 x1) (mulf x0 x2))
    then a.mul_distr <- None
    else a.is_strict <- false;

  ()


and force_poly ctx p =
  (* Restrict to the support of the polynomial. *)
  (* TODO: keep this as an invariant? *)
  Value.restrict_ring_poly p;
  (* Determine the variables used in the polynomial. *)
  let vars  = CCVector.create () in
  let polys = CCVector.create () in
  CCVector.iter (fun v -> match force ctx v with
      | StrRingPoly(q) ->
        CCVector.push polys (ZPoly.map_variables (fun x -> x + CCVector.size vars) q.poly);
        CCVector.append vars q.vars
      | v ->
        CCVector.push polys (ZPoly.variable (CCVector.size vars));
        CCVector.push vars v
    ) p.vars;
  (* Partition the variables. *)
  let part = partition ctx (ring_carrier ctx p.ring) vars in
  CCVector.map_in_place (ZPoly.map_variables (CCVector.get part.array)) polys;
  (* Compute the composed polynomial. *)
  p.vars <- CCVector.map (CCVector.get vars) part.repr;
  p.poly <- ZPoly.compose polys p.poly;
  (* Restrict to the support once more. *)
  Value.restrict_ring_poly p

and eval (ctx : Ctx.ctx) (env : Value.env) : Term.t -> Value.t = function
  | Var(i)  ->
    Value.env_lookup env i
  | Meta(m) ->
    Value.meta m
  | Top(i)  ->
    (Top.lookup ctx.gctx.tctx i).vtm
  | Let(t,u) ->
    eval (Ctx.extend ctx) (ESnoc(env,eval ctx env t)) u
  | Set -> Set
  | Pi(pl,a,b) ->
    Value.Pi(pl,eval ctx env a, Cl(env,b))
  | Lam(b)  ->
    Value.Lam(Cl(env,b))
  | App(f,ty,a) ->
    app ctx (eval ctx env f) (eval ctx env ty) (eval ctx env a)
  (* Record *)
  | RecordType(sg) ->
    RecordType(eval_record_sig ctx env sg)
  | Record(r) ->
    Record(eval_record ctx env r)
  | Proj(v,x) ->
    proj (eval ctx env v) x
  (* Prop *)
  | StrPropType ->
    StrPropType
  | StrProp(t) ->
    StrProp(str_prop ctx (eval ctx env t))
  | StrPropCarrier(t) ->
    str_prop_carrier ctx (eval ctx env t)
  | Bottom ->
    Value.Bottom
  (* Ring *)
  | StrRingType ->
    StrRingType
  | StrRing(t) ->
    StrRing(str_ring ctx (eval ctx env t))
  | StrRingCarrier(t) ->
    ring_carrier ctx (eval ctx env t)
  | StrRingConstant(t,z) ->
    ring_constant ctx (eval ctx env t) z
  | StrRingZero(t) ->
    ring_constant ctx (eval ctx env t) Z.zero
  | StrRingOne(t) ->
    ring_constant ctx (eval ctx env t) Z.one
  | StrRingAdd(t) ->
    ring_add ctx (eval ctx env t)
  | StrRingNeg(t) ->
    ring_neg ctx (eval ctx env t)
  | StrRingMul(t) ->
    ring_mul ctx (eval ctx env t)
  | ZZ -> ZZ
  (* OTT/SeTT *)
  | Eq(a,x,y) -> eq ctx (eval ctx env a) (eval ctx env x) (eval ctx env y)
  | Refl(a,x) -> Refl(eval ctx env a, eval ctx env x)
  | t ->
    Format.fprintf Format.std_formatter "TODO: %a@." Term.pp t;
    failwith __LOC__

and eval_record_sig ctx env : Term.record_sig -> Value.record_sig = function
  | SigEmpty -> SigEmpty
  | SigExtend(x,ty,sg) -> SigExtend(x,eval ctx env ty,Cl(env,sg))

and eval_record ctx env : Term.record -> Value.record = function
  | RecEmpty -> RecEmpty
  | RecExtend(x,ty,r) -> RecExtend(x,eval ctx env ty,eval_record ctx env r)

and app ctx (f : Value.t) ty (a : Value.t) = match f with
  | Var(i,sp) -> Var(i,SApp(sp,ty,a))
  | Meta(m,sp) -> Meta(m,SApp(sp,ty,a))
  | Lam(cl) -> clApp ctx cl a
  | _ -> failwith ("impossible: " ^ __LOC__)

and appSp ctx t : Value.spine -> Value.t = function
  | SEmpty -> t
  | SApp(sp,ty,a) -> app ctx (appSp ctx t sp) ty a
  | SProj(sp,x) -> proj (appSp ctx t sp) x
  | SAbsurd(sp) -> absurd ctx (appSp ctx t sp)

and clApp (ctx : Ctx.ctx) (cl : Value.tm_closure) (v : Value.t) : Value.t
  = match cl with
  | Cl(env,tm) -> eval ctx (ESnoc(env,v)) tm
  | ClFun(f) -> f v
  | ClConst(v) -> v

and sigClApp (ctx : Ctx.ctx) (cl : Value.sig_closure) (v : Value.t) : Value.record_sig
  = match cl with
  | Cl(env,sg) -> eval_record_sig ctx (ESnoc(env,v)) sg
  | ClFun(f) -> f v
  | ClConst(v) -> v

and appVar (ctx : Ctx.ctx) f ty x : Value.t
  = app ctx f ty (Value.var x)

and clAppVar (ctx : Ctx.ctx) (cl : Value.tm_closure) x : Value.t
  = clApp ctx cl (Value.var x)

and sigClAppVar (ctx : Ctx.ctx) (cl : Value.sig_closure) x : Value.record_sig
  = sigClApp ctx cl (Value.var x)

and record_proj (x : string) : Value.record -> Value.t = function
  | RecEmpty -> failwith ("impossible: " ^ __LOC__)
  | RecExtend(y,a,_) when x = y -> a
  | RecExtend(_,_,r) -> record_proj x r

and proj (v : Value.t) (x : string) : Value.t = match v with
  | Var(i,sp) -> Var(i,SProj(sp,x))
  | Meta(i,sp) -> Meta(i,SProj(sp,x))
  | Record(r) -> record_proj x r
  | _ -> failwith ("impossible " ^ __LOC__)

and proj_type (ctx : Ctx.ctx) (sg : Value.record_sig) (v : Value.t) (x : string)
  = match sg with
  | SigEmpty -> None
  | SigExtend(y,a,_) when x = y -> Some(a)
  | SigExtend(y,_,sg) -> proj_type ctx (sigClApp ctx sg (proj v y)) v x

and record_eta_expand (ctx : Ctx.ctx) (sg : Value.record_sig) (v : Value.t) =
  let rec go : Value.record_sig -> Value.record = function
    | SigEmpty -> RecEmpty
    | SigExtend(x,_,sg) ->
      let vx = proj v x in
      RecExtend(x,vx,go (sigClApp ctx sg vx))
  in
  match v with
  | Record(r) -> r
  | _ -> go sg

and str_prop ctx v : Value.str_prop =
  let a : Value.str_prop
    = { carrier = proj v "type";
        isProp = Some (proj v "isProp");
        is_strict = false;
      } in
  force_str_prop ctx a;
  a

and str_prop_carrier ctx v = match force ctx v with
  | StrProp(a) when a.is_strict -> a.carrier
  | _ -> StrPropCarrier(v)

and absurd ctx v = match force ctx v with
  | Var(i,sp) -> Var(i,SAbsurd(sp))
  | Meta(m,sp) -> Meta(m,SAbsurd(sp))
  | _ -> failwith ("impossible " ^ __LOC__)

(* StrRing *)

and str_ring ctx v : Value.str_ring =
  let a : Value.str_ring
    = { carrier = proj v "type";
        zero    = proj v "zero";
        one     = proj v "one";
        add     = proj v "add";
        neg     = proj v "neg";
        mul     = proj v "mul";
        add_assoc   = Some(proj v "add_assoc");
        add_comm    = Some(proj v "add_comm");
        add_neutral = Some(proj v "add_neutral");
        add_inverse = Some(proj v "add_inverse");
        mul_assoc   = Some(proj v "mul_assoc");
        mul_comm    = Some(proj v "mul_comm");
        mul_neutral = Some(proj v "mul_neutral");
        mul_distr   = Some(proj v "mul_distr");
        is_strict = false;
      } in
  (* force_str_ring ctx a; *)
  a

and ring_carrier ctx v = match force ctx v with
  (* | StrRing(a,_,_,_) -> a *)
  | _ -> StrRingCarrier(v)

and ring_constant ctx v z = match force ctx v with
  | v ->
    Value.StrRingPoly
      ({ ring = v;
         vars = CCVector.create ();
         poly = ZPoly.constant z;
       })

and ring_add ctx v = match force ctx v with
  | v ->
    Value.(lam (fun x -> lam (fun y ->
        StrRingPoly
          ({ ring = v;
             vars = CCVector.of_list [x;y];
             poly = ZPoly.generic_add;
           }))))

and ring_neg ctx v = match force ctx v with
  | v ->
    Value.(lam (fun x ->
        StrRingPoly
          ({ ring = v;
             vars = CCVector.of_list [x];
             poly = ZPoly.generic_neg;
           })))

and ring_mul ctx v = match force ctx v with
  | v ->
    Value.(lam (fun x -> lam (fun y ->
        StrRingPoly
          ({ ring = v;
             vars = CCVector.of_list [x;y];
             poly = ZPoly.generic_mul;
           }))))

(* OTT/SeTT *)

and eq ctx a x y = match (force ctx a, force ctx x, force ctx y) with
  | Set, Pi(pl1,a1,b1), Pi(pl2,a2,b2) ->
    if pl1 <> pl2
    then Bottom
    else
      mkStrProp
        (sigma (eq ctx Set a1 a2)
           (fun e -> Value.Pi(Expl, a2, ClFun(fun y ->
                let x = cast ctx a2 a1 (Sym(e)) y in
                eq ctx Set (clApp ctx b1 x) (clApp ctx b2 y)
              ))))
  | StrPropType, a, b ->
    let tyFwd = Value.Pi(Expl, StrPropCarrier(a), ClFun(fun _ -> StrPropCarrier(b))) in
    let tyBwd = Value.Pi(Expl, StrPropCarrier(b), ClFun(fun _ -> StrPropCarrier(a))) in
    mkStrProp (prod tyFwd tyBwd)
  | Pi(pl,a,b),f,g ->
    Value.(
      mkStrProp (pi pl a (fun x ->
          StrPropCarrier(eq ctx (clApp ctx b x) (app ctx f a x) (app ctx g a x)))))
  | a,x,y -> Eq(a,x,y)

and cast ctx a1 a2 e v : Value.t = match (force ctx a1, force ctx a2) with
  | Pi(pl1,a1,b1), Pi(pl2,a2,b2) when pl1 = pl2 ->
    Lam(ClFun(fun y ->
        let x = cast ctx a2 a1 (Value.Sym(proj e "fst")) y in
        cast ctx (clApp ctx b1 x) (clApp ctx b2 x) (app ctx (proj e "snd") a2 y) (app ctx v a1 x)))
  | a1,a2 when conv_bool ctx Value.Set a1 a2 -> v
  | a1,a2 -> Cast (a1,a2,e,v)

(* Conversion *)
and conv_bool ctx ty v1 v2 =
  try conv ctx ty v1 v2; true
  with ConvError -> false

and conv ctx ty v1 v2 = conv_ ctx (force ctx ty, force ctx v1, force ctx v2)

and conv_ ctx : Value.t * Value.t * Value.t -> unit = function
  (* metas *)
  | _, Meta(m1,sp1), Meta(m2,sp2) when m1 = m2 ->
    conv_spine ctx (sp1, sp2)

  (* Pi *)
  | _, Pi(pl1,a1,b1), Pi(pl2,a2,b2) when pl1 = pl2 ->
    conv ctx Set a1 a2;
    conv (Ctx.extend ctx) Set (clAppVar ctx b1 ctx.lvl) (clAppVar ctx b2 ctx.lvl)
  | Pi(_,a,b), t1, t2 ->
    conv (Ctx.extend ctx) (clAppVar ctx b ctx.lvl) (appVar ctx t1 a ctx.lvl) (appVar ctx t2 a ctx.lvl)

  (* Record *)
  | _, RecordType(sg1), RecordType(sg2) ->
    conv_record_sig ctx (sg1,sg2)
  | RecordType(sg), t1, t2 ->
    conv_record ctx (sg, record_eta_expand ctx sg t1, record_eta_expand ctx sg t2)

  (* Prop *)
  | _, StrPropType,StrPropType -> ()
  | StrPropType, a, b ->
    conv ctx (hPropType ctx) (str_prop_to_hProp ctx a) (str_prop_to_hProp ctx b)
  | _, StrPropCarrier(a), StrPropCarrier(b) ->
    conv ctx Set a b
  | StrPropCarrier(_), _, _ -> ()

  | _, Bottom, Bottom -> ()

  (* Ring *)
  | _, StrRingType, StrRingType -> ()
  | _, StrRingCarrier(a), StrRingCarrier(b) ->
    conv ctx Set a b
  | _, ZZ, ZZ -> ()
  | StrRingType, a, b ->
    conv ctx (ringType ctx) (str_ring_to_ring ctx a) (str_ring_to_ring ctx b)
  | StrRingCarrier(a), (StrRingPoly(_) as v1), v2
  | StrRingCarrier(a), v1, (StrRingPoly(_) as v2) ->
    conv_ring_poly ctx a v1 v2

  (* OTT / SeTT *)
  | _, Eq(a1,x1,y1), Eq(a2,x2,y2) ->
    conv ctx Set a1 a2;
    conv ctx a1 x1 x2;
    conv ctx a1 y1 y2

  (* Rigid *)
  | _, Var(x1,sp1), Var(x2,sp2) when x1 = x2 ->
    conv_spine ctx (sp1,sp2)
  | _, Set,Set -> ()

  (* fail *)
  | _,_,_ -> raise ConvError

and conv_spine ctx : Value.spine * Value.spine -> unit = function
  | SEmpty, SEmpty -> ()
  | SApp(sp1,a1,v1), SApp(sp2,_,v2) ->
    conv_spine ctx (sp1,sp2);
    conv ctx a1 v1 v2
  | SProj(sp1,x1), SProj(sp2,x2) when x1 = x2 ->
    conv_spine ctx (sp1,sp2)
  | _,_ -> raise ConvError

and conv_record_sig ctx = function
  | SigEmpty,SigEmpty -> ()
  | SigExtend(x,ty1,sg1),SigExtend(y,ty2,sg2) when x = y ->
    conv ctx Set ty1 ty2;
    conv_record_sig (Ctx.extend ctx) (sigClAppVar ctx sg1 ctx.lvl, sigClAppVar ctx sg2 ctx.lvl)
  | _,_ -> raise ConvError

and conv_record ctx = function
  | SigEmpty,RecEmpty,RecEmpty -> ()
  | SigExtend(_,ty,sg),RecExtend(_,a1,rc1),RecExtend(_,a2,rc2) ->
    conv ctx ty a1 a2;
    conv_record ctx (sigClApp ctx sg a1, rc1, rc2)
  | _,_,_ -> raise ConvError

and conv_ring_poly ctx a v1 v2 =
  Format.fprintf Format.std_formatter "%a@.%a@.%a@."
    Value.pp a
    Value.pp v1
    Value.pp v2;
  let p : Value.poly = { ring = a;
                         vars = CCVector.of_list [v1;v2];
                         poly = ZPoly.generic_sub;
                       } in
  force_poly ctx p;
  if not (ZPoly.is_zero p.poly) then raise ConvError

and partition (ctx : Ctx.ctx) (ty : Value.t) (p : Value.t CCVector.vector) : Partition.t =
  let part = Partition.create () in
  for i = 0 to CCVector.size p - 1 do
    let x = ref None in
    for j = 0 to part.sz-1 do
      if conv_bool ctx ty (CCVector.get p i) (CCVector.get p (CCVector.get part.repr j))
      then x := Some(j)
    done;
    Partition.push part !x
  done;
  part

(* Misc *)

and sigma a b : Value.t =
  Value.(
    RecordType(SigExtend("fst", a, ClFun(fun x ->
        SigExtend("snd", b x, ClConst(SigEmpty))))))

and prod a b : Value.t =
  Value.(
    RecordType(SigExtend("fst", a, ClConst(
        SigExtend("snd", b, ClConst(SigEmpty))))))


and pi pl a b =
  Value.Pi(pl, a, ClFun(b))

and arr pl a b = Value.Pi(pl, a, ClConst(b))

and mkStrProp ty : Value.t =
  Value.(
    StrProp({
        carrier = ty;
        isProp = None;
        is_strict = true;
      }))

and exists a b =
  let ty = sigma (StrPropCarrier(a)) (fun a -> StrPropCarrier(b a)) in
  mkStrProp ty

and forall a b =
  let ty = pi Expl (StrPropCarrier(a)) (fun a -> StrPropCarrier(b a)) in
  mkStrProp ty

and isProp ctx ty =
  pi Expl ty (fun x ->
      pi Expl ty (fun y ->
          str_prop_carrier ctx (eq ctx ty x y)
        ))

and hPropType ctx =
  Value.RecordType(SigExtend("type", Set, ClFun(fun ty ->
      SigExtend("isProp", isProp ctx ty, ClFun(fun _ ->
          SigEmpty)))))

and str_prop_to_hProp ctx v =
  Value.(
    let ty = str_prop_carrier ctx v in
    record [ ("type", ty)
           ; ("isProp", lam (fun x -> lam (fun _ -> Refl(ty,x))))
           ])

and str_ring_to_ring ctx v =
  Value.(
    let ty   = ring_carrier ctx v in
    let zero = ring_constant ctx v Z.zero in
    let one  = ring_constant ctx v Z.one in
    let add  = ring_add ctx v in
    let neg  = ring_add ctx v in
    let mul  = ring_add ctx v in
    let addf x y = app ctx (app ctx add ty x) ty y in
    let mulf x y = app ctx (app ctx mul ty x) ty y in
    record [ ("type", ty)
           ; ("zero", zero)
           ; ("one", one)
           ; ("add", add)
           ; ("neg", neg)
           ; ("mul", mul)
           ; ("add_assoc", lam (fun x -> lam (fun y -> lam (fun z -> Refl(ty, addf x (addf y z))))))
           ; ("add_comm", lam (fun x -> lam (fun y -> Refl(ty, addf x y))))
           ; ("add_neutral", lam (fun x -> Refl(ty, x)))
           ; ("add_inverse", lam (fun _ -> Refl(ty, zero)))
           ; ("mul_assoc", lam (fun x -> lam (fun y -> lam (fun z -> Refl(ty, mulf x (mulf y z))))))
           ; ("mul_comm", lam (fun x -> lam (fun y -> Refl(ty, mulf x y))))
           ; ("mul_neutral", lam (fun x -> Refl(ty, x)))
           ; ("mul_distr", lam (fun x -> lam (fun y -> lam (fun z -> Refl(ty, mulf x (addf y z))))))
           ])

(* def Ring = Record { *)
(*   type : Set; *)
(*   zero : type; *)
(*   one  : type; *)
(*   add  : type -> type -> type; *)
(*   neg  : type -> type; *)
(*   mul  : type -> type -> type; *)
(*   add_assoc : (x:type)(y:type)(z:type) -> Eq type (add x (add y z)) (add (add x y) z) .type; *)
(*   add_comm  : (x:type)(y:type) -> Eq type (add x y) (add y x) .type; *)
(*   add_neutral : (x:type) -> Eq type (add x zero) x .type; *)
(*   add_inverse : (x:type) -> Eq type (add x (neg x)) zero .type; *)
(*   mul_assoc : (x:type)(y:type)(z:type) -> Eq type (mul x (mul y z)) (mul (mul x y) z) .type; *)
(*   mul_comm : (x:type)(y:type) -> Eq type (mul x y) (mul y x) .type; *)
(*   mul_neutral : (x:type) -> Eq type (mul x one) x .type; *)
(*   mul_distr : (x:type)(y:type)(z:type) -> Eq type (mul x (add y z)) (add (mul x y) (mul x z)) .type; *)
(* }; *)
and ringType ctx =
  Value.(
    RecordType(SigExtend("type", Set, ClFun(fun ty ->
        SigExtend("zero", ty, ClFun(fun zero ->
            SigExtend("one", ty, ClFun(fun one ->
                SigExtend("add", arr Expl ty (arr Expl ty ty), ClFun(fun add ->
                    let addf x y = app ctx (app ctx add ty x) ty y in
                    SigExtend("neg", arr Expl ty ty, ClFun(fun neg ->
                        let negf x = app ctx neg ty x in
                        SigExtend("mul", arr Expl ty (arr Expl ty ty), ClFun(fun mul ->
                            let mulf x y = app ctx (app ctx mul ty x) ty y in
                            SigExtend("add_assoc", pi Expl ty (fun x -> pi Expl ty (fun y -> pi Expl ty (fun z ->
                                str_prop_carrier ctx (eq ctx ty (addf x (addf y z)) (addf (addf x y) z))))), ClConst(
                                SigExtend("add_comm", pi Expl ty (fun x -> pi Expl ty (fun y ->
                                    str_prop_carrier ctx (eq ctx ty (addf x y) (addf y x)))), ClConst(
                                    SigExtend("add_neutral", pi Expl ty (fun x ->
                                        str_prop_carrier ctx (eq ctx ty (addf x zero) x)), ClConst(
                                        SigExtend("add_inverse", pi Expl ty (fun x ->
                                            str_prop_carrier ctx (eq ctx ty (addf x (negf x)) zero)), ClConst(
                                            SigExtend("mul_assoc", pi Expl ty (fun x -> pi Expl ty (fun y -> pi Expl ty (fun z ->
                                                str_prop_carrier ctx (eq ctx ty (mulf x (mulf y z)) (mulf (mulf x y) z))))), ClConst(
                                                SigExtend("mul_comm", pi Expl ty (fun x -> pi Expl ty (fun y ->
                                                    str_prop_carrier ctx (eq ctx ty (mulf x y) (mulf y x)))), ClConst(
                                                    SigExtend("mul_neutral", pi Expl ty (fun x ->
                                                        str_prop_carrier ctx (eq ctx ty (mulf x one) x)), ClConst(
                                                        SigExtend("mul_distr", pi Expl ty (fun x -> pi Expl ty (fun y -> pi Expl ty (fun z ->
                                                            str_prop_carrier ctx (eq ctx ty (mulf x (addf y z)) (addf (mulf x y) (mulf x z)))))), ClConst(
                                                            SigEmpty))))))))))))))))))))))))))))))
