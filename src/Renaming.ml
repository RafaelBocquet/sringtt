open Common
module Ctx = EvalCtx

type t =
  { array   : int option CCVector.vector;
    pruning : bool CCVector.vector;
    mutable src : int;
    mutable tgt : int;
    mutable size : int;
    mutable cycles : MetaSet.t;
  }

let create src cycles : t =
  { array   = CCVector.make src None;
    pruning = CCVector.make 0 false;
    src     = src;
    tgt     = 0;
    size    = 0;
    cycles  = cycles;
  }

let weaken ren =
  CCVector.push ren.array None;
  ren.src <- ren.src+1

let drop ren =
  CCVector.push ren.pruning false;
  ren.size <- ren.size+1

let push ren i =
  CCVector.set ren.array i (Some ren.tgt);
  CCVector.push ren.pruning true;
  ren.size <- ren.size+1;
  ren.tgt <- ren.tgt+1

let is_trivial ren : bool =
  CCVector.for_all (fun x -> x = true) ren.pruning

let pp fmt (pren : t) =
  CCVector.iter (function
      | None -> Format.fprintf fmt "-"
      | Some(x) -> Format.fprintf fmt "%d" x
    ) pren.array;
  Format.fprintf fmt "|";
  CCVector.iter (fun x ->
      Format.fprintf fmt "%d" (if x then 1 else 0)
    ) pren.pruning

exception RenameError

let rec rename (ectx : Ctx.ctx) (pren : t) v =
  rename_ ectx pren (Eval.force ectx v)
and rename_var (ectx : Ctx.ctx) pren x =
  if x < pren.src then match CCVector.get pren.array x with
    | None -> raise RenameError
    | Some(y) -> lvl_to_idx (pren.tgt + ectx.lvl-pren.src) y
  else lvl_to_idx (pren.tgt + ectx.lvl-pren.src) (x+pren.tgt-pren.src)
and rename_ (ectx : Ctx.ctx) pren : Value.t -> Term.t = function
  | Var(x,sp) ->
    rename_spine ectx pren (Term.Var(rename_var ectx pren x)) sp
  | Meta(m,_) when MetaSet.mem m pren.cycles ->
    raise RenameError
  | Meta(m,sp) ->
    rename_meta_spine ectx pren m sp
  | Set ->
    Set
  | Pi(pl,a,b) ->
    Pi(pl,rename ectx pren a, rename_closure ectx pren b)
  | Lam(b) ->
    Lam(rename_closure ectx pren b)
  | RecordType(sg) ->
    RecordType(rename_record_sig ectx pren sg)
  | Record(rc) ->
    Record(rename_record ectx pren rc)
  (* StrProp *)
  | StrPropType ->
    StrPropType
  | StrProp(a) ->
    let carrier = a.carrier in
    let isProp  = match a.isProp with
      | None -> Value.(lam (fun x -> lam (fun _ -> Refl(carrier,x))))
      | Some(x) -> x
    in
    StrProp(Record(RecExtend("type", rename ectx pren carrier,
                             RecExtend("isProp", rename ectx pren isProp,
                                       RecEmpty))))
  | StrPropCarrier(a) ->
    StrPropCarrier(rename ectx pren a)
  | Bottom ->
    Bottom
  (* StrRing *)
  | StrRingType ->
    StrRingType
  | StrRingCarrier(a) ->
    StrRingCarrier(rename ectx pren a)
  | ZZ ->
    ZZ
and rename_closure ectx pren cl =
  rename (Ctx.extend ectx) pren (Eval.clAppVar (Ctx.extend ectx) cl ectx.lvl)
and rename_record_sig_closure ectx pren cl =
  rename_record_sig (Ctx.extend ectx) pren (Eval.sigClAppVar (Ctx.extend ectx) cl ectx.lvl)

and rename_record_sig ectx pren = function
  | SigEmpty ->
    SigEmpty
  | SigExtend(x,ty,sg) ->
    SigExtend(x, rename ectx pren ty, rename_record_sig_closure ectx pren sg)

and rename_record ectx pren = function
  | RecEmpty ->
    RecEmpty
  | RecExtend(x,t,rc) ->
    RecExtend(x, rename ectx pren t, rename_record ectx pren rc)

and rename_spine (ectx : Ctx.ctx) pren t = function
  | SEmpty -> t
  | SApp(sp,ty,a) ->
    Term.App(rename_spine ectx pren t sp, rename ectx pren ty, rename ectx pren a)
  | SProj(sp,x) ->
    Term.Proj(rename_spine ectx pren t sp, x)
  | SAbsurd(sp) ->
    Term.Absurd(rename_spine ectx pren t sp)

and rename_meta_spine ectx (pren : t) (m:meta) sp =
  let pr = create ectx.lvl MetaSet.empty in
  let exception DontPrune in
  let rec go : Value.spine -> unit = function
    | SEmpty -> ()
    | SApp(s,_,v) ->
      begin match Eval.force ectx v with
        | Var(x, SEmpty) ->
          go s;
          if x >= pren.src || Option.is_some (CCVector.get pren.array x)
          then push pr x
          else drop pr
        | _ -> raise DontPrune
      end
    | SProj(_) -> raise DontPrune
    | SAbsurd(_) -> raise DontPrune
  in
  try
    go sp;
    if is_trivial pr then raise DontPrune;
    prune_meta ectx.gctx m pr;
    rename ectx pren (Meta(m,sp))
  with DontPrune ->
    rename_spine ectx pren (Term.Meta(m)) sp

and prune_type (ctx : Global.ctx) (pr : t) mty : Term.t * (lvl * Term.t) list =
  let ren = create 0 MetaSet.empty in
  let rec go (ectx : Ctx.ctx) a =
    if ectx.lvl = pr.size
    then rename ectx ren a, []
    else match CCVector.get pr.pruning ectx.lvl, Eval.force ectx a with
    | true, Value.Pi(pl,a,b) ->
      let ra = rename ectx ren a in
      weaken ren;
      push ren ectx.lvl;
      let ty, vs = go (Ctx.extend ectx) (Eval.clAppVar (Ctx.extend ectx) b ectx.lvl) in
      Term.Pi(pl,ra,ty), (ectx.lvl, ra) :: vs
    | false, Value.Pi(_,_,b) ->
      weaken ren;
      go (Ctx.extend ectx) (Eval.clAppVar (Ctx.extend ectx) b ectx.lvl)
    | _ -> failwith ("impossible " ^ __LOC__)
  in
  go (Ctx.create ctx 0) mty

and prune_meta (ctx : Global.ctx) (m : meta) (pr : t) =
  let ectx = Ctx.create ctx 0 in
  let solty, vs = prune_type ctx pr (Meta.get_type ctx.mctx m) in
  let newm = Meta.fresh_meta ctx.mctx (Eval.eval ectx EEmpty solty) in
  let sol = Term.lams (List.fold_right (fun (x,ty) t -> Term.App(t, ty, Term.Var(lvl_to_idx pr.size x))) vs (Term.Meta(newm))) pr.size in
  Meta.solve_meta ctx.mctx m sol (Eval.eval ectx EEmpty sol)
